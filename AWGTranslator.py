import pandas as pd
import numpy as np
import sys

df = pd.read_csv(sys.argv[1],index_col=False,usecols=[1])
df.columns = ['data']
max = df['data'].max()
min = max * (-1)

df['data'] =(((df['data'] - min)/(max - min)) * 4095).astype(int)


pre = [0x7261, 0x0062, 0x1100, 0x0000]
data = df['data'].to_numpy()

if len(data) < 4096:
  target_length = (4096 - len(data))
  start = 0
  min_dist = np.infty
  target_vector = data[len(data)-target_length:]
  for i in range(0,len(data)-target_length):
    current_vector = data[i:i+target_length]
    distance = np.linalg.norm(target_vector - current_vector)
    if distance < min_dist:
      min_dist = distance
      start = i

  padd = data[(start+target_length):(start+2*target_length)]
elif len(data) > 4096:
  # data = data[:4095]
  start = 0
  min_dist = np.infty
  target_vector = data[:256]
  for i in range(256,(4095-256)):
    current_vector = data[i:i+256]
    distance = np.linalg.norm(target_vector - current_vector)
    if (distance < min_dist):
      min_dist = distance
      start = i
  data = data[:(start+256)]
  padd = data[256:(4096-len(data)+256)]
else:
  padd = np.array([])

array = np.concatenate((pre,data,padd))
bytes = np.ascontiguousarray(array, dtype='i2').tobytes()


file = open('.'.join(sys.argv[1].split('.')[:-1]) + ".arb", "wb")

file.write(bytes)
